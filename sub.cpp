//
//  Synchronized subscriber in C++
//

#include "zmq.hpp"
#include "generated/message.pb.h"
#include <iostream>

int main(int argc, char *argv[])
{
    zmq::context_t context(1);

    //  First, connect our subscriber socket
    zmq::socket_t subscriber(context, zmq::socket_type::sub);
    subscriber.connect("tcp://localhost:5561");
    subscriber.set(zmq::sockopt::subscribe, "");

    //  Second, synchronize with publisher
    zmq::socket_t syncclient(context, zmq::socket_type::req);
    syncclient.connect("tcp://localhost:5562");

    //  - send a synchronization request
    syncclient.send(zmq::str_buffer(""), zmq::send_flags::none);

    //  - wait for synchronization reply
    zmq::message_t sync_zmsg;
    (void)syncclient.recv(sync_zmsg);

    //  Third, get our updates and report how many we got
    int update_nbr = 0;
    zmq::message_t zmsg;
    while (1)
    {
        (void)subscriber.recv(zmsg, zmq::recv_flags::none);
        std::string serialized(static_cast<char *>(zmsg.data()), zmsg.size());
        Info info;
        info.ParseFromString(serialized);
        if (serialized.compare("END") == 0)
        {
            break;
        }
        std::cout << "Receiving id: " << info.id() << " date: " << info.date();
        for (auto info_msg : info.messages())
        {
            std::cout << "  Type: " << info_msg.type() << " Message: " << info_msg.text() << "\n";
        }
        update_nbr++;
    }
    std::cout << "Received " << update_nbr << " updates" << std::endl;

    return 0;
}