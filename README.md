# IPC with ZeroMQ and Protocol Buffers

This code repository shows how to implement ZeroMQ and Protobuf with a simple example.

## Requirements

Installing `libzmq` and `libprotobuf` on Ubuntu:

``` bash
sudo apt install libzmq3-dev libprotobuf-dev
```

## Compiling

If you change the message.proto file, please update the generated files by compiling the file:

```
protoc --cpp_out=. message.proto
```

Then, compiling the C++ codes:

```
g++ pub.cpp generated/message.pb.cc -o pub -lzmq -lprotobuf -Wall
g++ sub.cpp generated/message.pb.cc -o sub -lzmq -lprotobuf -Wall
```

Finally, run the both executables in two different terminals.
