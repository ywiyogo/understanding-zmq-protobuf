//
//  Synchronized publisher in C++
//

#include "zmq.hpp"
#include "generated/message.pb.h"
#include <iostream>

//  We wait for 10 subscribers
#define SUBSCRIBERS_EXPECTED 1

int main()
{
    zmq::context_t ctx;

    zmq::context_t context(1);
    //  Socket to talk to clients
    zmq::socket_t publisher(context, zmq::socket_type::pub);

    int sndhwm = 0;
    publisher.set(zmq::sockopt::sndhwm, sndhwm);

    publisher.bind("tcp://*:5561");

    //  Socket to receive signals
    zmq::socket_t syncservice(context, zmq::socket_type::rep);
    syncservice.bind("tcp://*:5562");

    //  Get synchronization from subscribers
    int subscribers = 0;
    zmq::message_t sync_zmsg;
    while (subscribers < SUBSCRIBERS_EXPECTED)
    {
        std::cout << "Wait for synchronization request\n";
        (void)syncservice.recv(sync_zmsg);

        std::cout << "Send synchronization reply\n";
        (void)syncservice.send(sync_zmsg, zmq::send_flags::none);

        subscribers++;
    }

    // Create a Protobuf message and serialize it

    //  Now broadcast exactly 1M updates followed by END
    std::string serialized;
    Info::Message *info_msg;
    for (auto i = 0; i < 10; i++)
    {
        Info info;
        info.set_date("10.10.2023");
        info.set_id(i);
        info_msg = info.add_messages();
        info_msg->set_text("Hello, ZMQ & Protobuf!");
        info_msg->set_type(Info_MessageType::Info_MessageType_DEBUG);

        info.SerializeToString(&serialized);
        zmq::message_t zmsg(serialized.size());
        memcpy(zmsg.data(), serialized.data(), serialized.size());
        (void)publisher.send(zmsg, zmq::send_flags::none);
    }

    publisher.send(zmq::str_buffer("END"), zmq::send_flags::none);

    std::cout.flush();
    return 0;
}